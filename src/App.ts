
import * as express from 'express';

class App 
{
    public express;

    constructor() {
        this.express = express();
        this.mountRoutes();
    }

    private mountRoutes(): void {
        this.express.get('/', (req, res) => {
            res.json({
                message: 'Ciao World!'
            });
        });

        this.express.get('/about', (req, res) => {
            res.json({
                message: "Ciao sono Gerry, l'autore di questo progettino"
            });
        });

        // parametri in GET
        // esempio: http://localhost:3000/testget?param1=hola&param2=amici
        this.express.get('/testget', (req, res) => {
            let response = {
                param1: req.query.param1,
                param2: req.query.param2
            };
            
            //res.end(JSON.stringify(response));
            res.json(response);
        });        
    }

    /* utilizzo dell'oggetto Router
    private mountRoutes(): void {
        const router = express.Router();
        
        router.get('/', (req, res) => {
            res.json({
                message: 'Hello World!'
            });
        })

        this.express.use('/', router);
    }
    */
}

export default new App().express;
